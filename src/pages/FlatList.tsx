import React from 'react'
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import FlatItemList from '../components/FlatItemList';

const FlatList: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Mes biens</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Mes biens</IonTitle>
          </IonToolbar>
        </IonHeader>
        <FlatItemList name="Mes biens" />
      </IonContent>
    </IonPage>
  );
};

export default FlatList;
