import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import FlatItemList from '../components/FlatItemList';

const Tab2: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 2</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 2</IonTitle>
          </IonToolbar>
        </IonHeader>
        <FlatItemList name="Tab 2 page" />
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
