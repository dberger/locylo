import React from 'react';
import {
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardTitle, IonCol,
    IonContent, IonGrid,
    IonIcon, IonLabel,
    IonPage, IonRow,
} from "@ionic/react";
import {
    barChartOutline,
    cardOutline, fitnessOutline,
    key,
    mapOutline,
    person
} from "ionicons/icons";
import {PaymentTable} from "../components/PaymentTable";
import {useParams} from "react-router";
import {useFetch} from "../hooks/fetch";
import moment from "moment";

type Props = {};
export const FlatDetails = (props: Props) => {
    let {id}: any = useParams();
    const {data, fetchError} : any = useFetch({url: `/api/flat_models/` + id}); // TODO GESTION ERREUR

    return (
        <> {data && <IonPage>
            <IonContent fullscreen>
                <img alt={`${data?.address?.address}, ${data?.address?.city}, ${data?.address?.zip}`} className={'object-fit-cover h-200 w-100'}
                     src={'https://img.leboncoin.fr/api/v1/lbcpb1/images/a8/7f/57/a87f57a7a93cea950875cd9386df7b5572c710cb.jpg?rule=ad-large'}/>
                <div className={'flex flex-col'}>
                    <p className={'ml-20 mb-0 ion-card-title'}>{data.name}</p>
                    <div className={'flex align-items-center'}>
                        <p className={'ml-20 ion-card-subtitle'}>{`${data?.address?.address}, ${data?.address?.city}, ${data?.address?.zip}`}</p>
                        <IonIcon className={'ml-5 icon__success'} icon={mapOutline}/>
                    </div>
                </div>
                <IonCard key={key}>
                    <IonCardHeader>
                        <IonCardTitle className={'flex align-items-center'}>
                            <IonIcon icon={person}/>
                            <span className={'ml-5'}>Informations sur le locataire</span>
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        <IonGrid>
                            <IonRow>
                                <IonCol className={'flex align-items-center'}>
                                    Nom : {data?.tenant.lastname}
                                </IonCol>
                                <IonCol className={'flex align-items-center'}>
                                    Tél : {data?.tenant.phone}
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol className={'flex align-items-center'}>
                                    Prénom : {data?.tenant.firstname}
                                </IonCol>
                                <IonCol className={'flex align-items-center'}>
                                    Mail : {data?.tenant.mail}
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol className={'flex align-items-center'}>
                                    <p className={'ml-5'}>Assuré jusqu'au <strong>{moment(data?.insuredUntil).format('DD/MM/YYYY')}</strong></p>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonCardContent>
                </IonCard>
                <IonCard key={key}>
                    <IonCardHeader>
                        <IonCardTitle className={'flex align-items-center'}>
                            <IonIcon icon={cardOutline}/>
                            <span className={'ml-5'}>Suivi des loyers</span>
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        <div className={'flex flex-col'}>
                            <IonLabel className={'ml-5'}>Montant du loyer : {data?.rentalDetails?.price} € + {data?.rentalDetails?.chargesAmount} € de charges</IonLabel>
                            <PaymentTable rows={data?.payments}/>
                        </div>
                    </IonCardContent>
                </IonCard>
                <IonCard key={key}>
                    <IonCardHeader>
                        <IonCardTitle className={'flex align-items-center'}>
                            <IonIcon icon={barChartOutline}/>
                            <span className={'ml-5'}>Statistiques</span>
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent className={'flex flex-col'}>
                        <IonLabel className={'ml-5'}>Date acquisition : {moment(data?.purchaseDate).format('DD/MM/YYYY')}</IonLabel>
                        <IonLabel className={'ml-5'}>Prix acquisition : {parseFloat(data?.flatPrice).toFixed(2)} €</IonLabel>
                        <IonLabel className={'ml-5'}>Revenu généré : {parseFloat(data?.sumRent).toFixed(2)} €</IonLabel>
                    </IonCardContent>
                </IonCard>
            </IonContent>
        </IonPage>}
        </>
    );
}
;
