import {Redirect, Route} from 'react-router-dom';
import {
    IonApp,
    IonIcon,
    IonLabel,
    IonRouterOutlet,
    IonTabBar,
    IonTabButton,
    IonTabs,
} from '@ionic/react';
import {IonReactRouter} from '@ionic/react-router';
import {
    analyticsOutline,
    businessOutline,
    constructOutline,
    ellipse,
    square,
    statsChartOutline,
    triangle
} from 'ionicons/icons';
import FlatList from './pages/FlatList';
import Tab2 from './pages/Tab2';
import Tab3 from './pages/Tab3';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import './assets/scss/base.scss'
import {createMuiTheme, CssBaseline, useMediaQuery, ThemeProvider} from "@material-ui/core";
import React from "react";
import {FlatDetails} from "./pages/FlatDetails";

const App: React.FC = () => {

    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

    const theme = React.useMemo(
        () =>
            createMuiTheme({
                palette: {
                    type: prefersDarkMode ? 'dark' : 'light',
                },
            }),
        [prefersDarkMode],
    );
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline/>
            <IonApp>
                <IonReactRouter>
                    <IonTabs>
                        <IonRouterOutlet>
                            <Route exact path="/my-goods" component={FlatList}/>
                            <Route path="/my-goods/:id" component={FlatDetails}/>
                            <Route exact path="/stats">
                                <Tab2/>
                            </Route>
                            <Route path="/settings">
                                <Tab3/>
                            </Route>
                            <Route exact path="/">
                                <Redirect to="/my-goods"/>
                            </Route>
                        </IonRouterOutlet>
                        <IonTabBar slot="bottom">
                            <IonTabButton tab="goods" href="/my-goods">
                                <IonIcon icon={businessOutline}/>
                                <IonLabel>Mes biens</IonLabel>
                            </IonTabButton>
                            <IonTabButton tab="stats" href="/stats">
                                <IonIcon icon={statsChartOutline}/>
                                <IonLabel>Statistiques</IonLabel>
                            </IonTabButton>
                            <IonTabButton tab="settings" href="/settings">
                                <IonIcon icon={constructOutline}/>
                                <IonLabel>Configuration</IonLabel>
                            </IonTabButton>
                        </IonTabBar>
                    </IonTabs>
                </IonReactRouter>
            </IonApp>
        </ThemeProvider>
    );
}

export default App;
