export const FLATS = [
    {
        "id": 2,
        "name": "Appartement T2bis 63m²",
        "rentedSince": "2020-08-18T00:00:00+00:00",
        "lastRentPaidOn": "2021-05-31T00:00:00+00:00",
        "insuredUntil": "2021-08-18T00:00:00+00:00",
        "rentPrice": 495,
        "address": {
            "id": 2,
            "address": "4 rue des creuses",
            "zip": "42000",
            "city": "Saint-Etienne"
        }
    }
]
