export const FAKE_RENTS = [
    {
        "month": "Janvier 2021",
        "amount": "495.00 €",
        "paymentDate": "31/01/21",
        "sendAt": "31/01/21"
    },
    {
        "month": "Février 2021",
        "amount": "495.00 €",
        "paymentDate": "28/02/21",
        "sendAt": "28/02/21"
    },
    {
        "month": "Mars 2021",
        "amount": "495.00 €",
        "paymentDate": "31/03/21",
        "sendAt": "31/03/21"
    },
    {
        "month": "Avril 2021",
        "amount": "495.00 €",
        "paymentDate": "31/04/21",
        "sendAt": null
    },
    {
        "month": "Mai 2021",
        "amount": "495.00 €",
        "paymentDate": null,
        "sendAt": null
    }
];
