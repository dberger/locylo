import {useState, useEffect} from 'react';

export const useFetch = ({url, method = 'GET', body = {}}) => {
    const [data, setData] = useState(null);
    const [fetchError, setError] = useState(null);
    const handleErrors = (response) => {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        return response;
    }
    useEffect(() => {
        let fetchParams = {
            method: method
        };
        if (Object.keys(body).length > 0) {
            fetchParams.body = JSON.stringify(body)
        }
        fetch(process.env.REACT_APP_BACK_URL + url, fetchParams)
            .then(handleErrors)
            .then(data => data.json()
                .then(json => {
                    setData(json['hydra:member'] ? json['hydra:member'] : json)
                })).catch(err => {
            if (err.json) {
                err.json()
                    .then(setError(err))
                    .catch(err => {
                        setError(err)
                    })
            } else {
                setError(err)
            }
        })
    }, []);

    return {data, fetchError};
}
