import React from 'react';
import {cardOutline, fitnessOutline, key, mapOutline, storefrontOutline, timeOutline} from "ionicons/icons";
import {
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle, IonCol,
    IonGrid,
    IonIcon, IonLabel, IonRow,
    IonSkeletonText
} from "@ionic/react";
import moment from "moment";

type Props = {
    flat: any,
    isLoading: boolean
};
export const FlatItem = (props: Props) => {
    const {flat, isLoading} = props;

    return (
        <IonCard key={key} routerLink={'/my-goods/' + flat?.id}>
            <IonCardHeader>
                <IonCardTitle>{isLoading ? <IonSkeletonText animated style={{
                    width: '100%',
                    height: '30px'
                }}/> : flat?.name}</IonCardTitle>
                <IonCardSubtitle>{isLoading ? <IonSkeletonText animated style={{
                        width: '100%',
                        height: '20px'
                    }}/> :
                    <div className={'flex align-items-center'}>
                        {`${flat?.address?.address}, ${flat?.address?.city}, ${flat?.address?.zip}`}
                        <IonIcon className={'ml-5 icon__success'} icon={mapOutline}/>
                    </div>
                }</IonCardSubtitle>
            </IonCardHeader>

            <IonCardContent>
                {isLoading ?
                    <IonSkeletonText animated style={{width: '100%', height: '300px'}}/>
                    :
                    <>
                        <img alt={`${flat?.address?.address}, ${flat?.address?.city}, ${flat?.address?.zip}`}
                             className={'gif__webm'}
                             src={'https://img.leboncoin.fr/api/v1/lbcpb1/images/a8/7f/57/a87f57a7a93cea950875cd9386df7b5572c710cb.jpg?rule=ad-large'}/>
                        <IonGrid>
                            <IonRow>
                                <IonCol className={'flex align-items-center'}>
                                    <IonIcon className={'icon__success'} icon={storefrontOutline}/>
                                    <IonLabel className={'ml-5'}>Loué depuis
                                        le {moment(flat?.rentedSince).format('DD/MM/YYYY')}</IonLabel>
                                </IonCol>
                                <IonCol className={'flex align-items-center'}>
                                    <IonIcon className={'icon__success'} icon={timeOutline}/>
                                    <IonLabel className={'ml-5'}>Loyer payé
                                        le {moment(flat?.lastRentPaidOn).format('DD/MM/YYYY')}</IonLabel>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol className={'flex align-items-center'}>
                                    <IonIcon className={'icon__success'} icon={cardOutline}/>
                                    <IonLabel className={'ml-5'}>{flat?.rentPrice} €</IonLabel>
                                </IonCol>
                                <IonCol className={'flex align-items-center'}>
                                    <IonIcon className={'icon__success'} icon={fitnessOutline}/>
                                    <IonLabel className={'ml-5'}>Assuré
                                        jusqu'au {moment(flat?.insuredUntil).format('DD/MM/YYYY')}</IonLabel>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </>
                }
            </IonCardContent>
        </IonCard>
    );
}
;
