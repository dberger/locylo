
import {useEffect, useState} from "react";
import {useFetch} from "../hooks/fetch";
import {FlatItem} from "./FlatItem";
import {FLATS} from "../api/local/flats";

interface ContainerProps {
    name: string;
}

const FlatItemList: React.FC<ContainerProps> = ({}) => {
    const [isLoading, setLoading] = useState(true);
    const {data, fetchError} : any = useFetch({url: `/api/flat_models`}); // TODO GESTION ERREUR
    const [flats, setFlats] = useState(FLATS);

    useEffect(() => {
        if (data) {
            setLoading(false)
            setFlats(data)
        }
    }, [data]);

    return (
        <>
            {flats && Array.isArray(flats) && flats.map((item : any, key:number) => <FlatItem key={key} flat={item} isLoading={isLoading}/>)}
        </>
    );
};

export default FlatItemList;
