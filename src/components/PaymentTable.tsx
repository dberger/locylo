import React, {useEffect, useState} from 'react';
import Paper from "@material-ui/core/Paper/Paper";
import {
    createStyles, Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Theme,
    withStyles
} from "@material-ui/core";
import {IonButton, useIonAlert} from "@ionic/react";
import moment from "moment";
import {useFetch} from "../hooks/fetch";

type Props = {
    rows: any
};

export const PaymentTable = (props: Props) => {
    const [present] = useIonAlert();
    const [payments,setPayments] = useState<any[]>([]);

    useEffect(() => {
       if(!payments.length){
           setPayments(props.rows);
       }
    }, );


    const StyledTableCell = withStyles((theme: Theme) =>
        createStyles({
            head: {
                backgroundColor: 'var(--ion-background-color, #fff)',
                color: theme.palette.common.white,
            },
            body: {
                fontSize: 14,
            },
        }),
    )(TableCell);

    const StyledTableRow = withStyles((theme: Theme) =>
        createStyles({
            root: {
                '&:nth-of-type(odd)': {
                    backgroundColor: 'var(--ion-card-background, var(--ion-item-background, var(--ion-background-color, #fff)))',
                },
            },
        }),
    )(TableRow);

    const handleClickPayment = (row: any) => {
        present({
            header: 'Alert',
            message: 'Êtes-vous sûr de vouloir enregistrer un paiement pour cette période ?',
            buttons: [
                'Annuler',
                {
                    text: 'Oui', handler: () => {
                        fetch(process.env.REACT_APP_BACK_URL + `/api/tenant_payment_models/${row.id}`, {
                            method: 'PATCH',
                            body: JSON.stringify({
                                id: row.id,
                                paymentDate: moment().format()
                            }),
                            headers: {
                                'Content-type' : 'application/merge-patch+json'
                            }
                        })
                            .then(data => data.json())
                            .then((json) => {
                                const index = payments.findIndex((payment:any) => payment.id === row.id);
                                payments[index].paymentDate = json.paymentDate;
                                setPayments([...payments])
                            })
                            .catch(err => {
                                console.log(err)
                            })
                    }
                },
            ],
        })
    }

    const handleClickReceipt = (row: any) => {
        present({
            header: 'Alert',
            message: 'Êtes-vous sûr de vouloir envoyer une quittance de loyer pour cette période ?',
            buttons: [
                'Annuler',
                {
                    text: 'Oui', handler: () => {
                        fetch(process.env.REACT_APP_BACK_URL + `/api/tenant_payment_models/${row.id}`, {
                            method: 'PATCH',
                            body: JSON.stringify({
                                id: row.id,
                                receiptSendAt: moment().format()
                            }),
                            headers: {
                                'Content-type' : 'application/merge-patch+json'
                            }
                        })
                            .then(data => data.json())
                            .then((json) => {
                                const index = payments.findIndex((payment:any) => payment.id === row.id);
                                payments[index].receiptSendAt = json.receiptSendAt;
                                setPayments([...payments])
                            })
                            .catch(err => {
                                console.log(err)
                            })
                    }
                },
            ],
        })
    }

    return (
        <TableContainer className='mt-15' component={Paper}>
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell>Mois</StyledTableCell>
                        <StyledTableCell align="right">Montant</StyledTableCell>
                        <StyledTableCell align="right">Date de paiement</StyledTableCell>
                        <StyledTableCell align="right">Quittance</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {payments.map((row: any) => (
                        <StyledTableRow key={row.month}>
                            <StyledTableCell component="th" scope="row">
                                {row.debtDate ? moment(row.debtDate).format('MMM YYYY') : '-'}
                            </StyledTableCell>
                            <StyledTableCell align="right">{parseFloat(row.amountPaid).toFixed(2)} €</StyledTableCell>
                            <StyledTableCell
                                align="right">{row.paymentDate ? moment(row.paymentDate).format('DD/MM/YYYY') :
                                <IonButton onClick={() =>
                                    handleClickPayment(row)
                                } size={'small'} color="success">Paiement reçu ?</IonButton>}</StyledTableCell>
                            <StyledTableCell
                                align="right">{row.receiptSendAt ? moment(row.receiptSendAt).format('DD/MM/YYYY') : row.paymentDate &&
                                <IonButton onClick={() =>
                                    handleClickReceipt(row)
                                } size={'small'} color="primary">Envoyer</IonButton>}</StyledTableCell>
                        </StyledTableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};
